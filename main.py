#!/usr/bin/python3
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# main.py
#

# Custom libraries
from builder import MediaCreator

# Generic libraries
import sys

# Third-party libraries
from PyQt6.QtWidgets import QApplication


def main(arguments: list[str]) -> int:
	"""
	"""
	application: QApplication = QApplication(arguments)

	window = MediaCreator()
	window.show()

	return application.exec()


if __name__ == '__main__':
	sys.exit(main(sys.argv))
