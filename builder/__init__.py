#!/usr/bin/python3
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# builder/__init__.py
#

# Custom libraries
from extract_image import Image

# Generic libraries
from hashlib import md5, sha256
from math import ceil
from os import getcwd, makedirs
from os.path import abspath, basename, dirname, exists, join, splitext
from re import search, sub
from shutil import copyfile
from urllib.request import urlopen, urlretrieve
import io

# Third-party libraries
from gnupg import GPG
from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication, QWidget
from yaml import YAMLError, safe_load

# Key used to download Ubuntu's signing key
identity_key: str = '843938DF228D22F7B3742BC0D94AA3F0EFE21092'


class MediaCreator(QWidget):
	# Path to extracted files
	extract_path: str

	# Online location from where the images can be downloaded
	source_path: str

	# Directory to where all data will be stored
	target_path: str

	def __init__(self) -> None:
		super(MediaCreator, self).__init__()
		loadUi(join(dirname(__file__), 'form.ui'), self)

		# Ubuntu versions to choose from
		versions: tuple[str, ...] = ('bionic', 'focal', 'jammy')

		self.user_edit.setText(getcwd())
		self.target_edit.setText(getcwd())
		self.version_list.addItems(versions)
		self.version_list.setCurrentText(versions[-1])

		self.action_button.clicked.connect(self.run)

	def file_download(self, name: str) -> str:
		"""
		Downloads a file at the resource path & stores it in the specified directory. Then, returns
		the path to the file.
		"""
		path: str = join(self.target_path, name)

		self.status_label.setText(f'Downloading {name}...')
		response = urlopen(join(self.source_path, name))

		with open(path, 'w') as file:
			file.write(response.read().decode('utf-8'))

		return path

	def file_find(self, name: str, target: str) -> bool:
		"""
		Opens a file & searches it for a specific string. If the string is found in the file the
		function returns True. Otherwise, False.
		"""
		with open(join(self.extract_path, name), 'r') as file:
			return target in file.read()

	def file_replace(self, name: str, target: str, substitute: str):
		"""
		Make an in-place replacement of the specified target string, a regular expression, with a
		specified substitute string in a given file.
		"""
		with open(join(self.extract_path, name), 'r+') as file:
			lines: list[str] = file.readlines()

			# Replace target with substitute on every line
			# map(lambda line: line.replace(target, substitute), lines)
			map(lambda line: sub(target, substitute, line), lines)

			# Erase file content
			file.truncate(0)

			# Write new content
			file.writelines(lines)

	def file_validate(self, path: str) -> bool:
		"""
		Checks whether the user-data file is found & that it contains valid YAML code. Returns True
		if the validation passes. Otherwise, False.
		"""
		self.status_label.setText('Validating user-data file...')

		try:
			with open(path, 'r') as file:
				safe_load(file)
		except (FileNotFoundError, YAMLError):
			return False
		return True

	def image_download(self, name: str) -> str:
		"""
		Downloads an image at the resource path & stores it in the specified directory. Then,
		returns the path to the image.
		"""
		path: str = join(self.target_path, name)

		if not exists(path):
			self.status_label.setText(f'Downloading {name}...')
			urlretrieve(join(self.source_path, name), path, self.report_hook)

		return path

	@staticmethod
	def image_hash(path: str) -> str:
		"""
		Generates a hash using the SHA256 algorithm which is later used to verify the image's
		integrity.
		"""
		_hash = sha256()
		with open(path, 'rb') as file:
			while True:
				buffer: bytes = file.read(io.DEFAULT_BUFFER_SIZE)
				if not buffer:
					break

				_hash.update(buffer)
		return _hash.hexdigest()

	def image_verify(self, checksum: str, path: str) -> bool:
		"""
		Matches a line in the checksum file with the digest created from the image file effectively
		verifying the content of the image is intact.
		"""
		digest: str = self.image_hash(path)
		name: str = basename(path)

		with open(checksum, 'r') as file:
			for line in file.readlines():
				if f'{digest} *{name}\n' == line:
					return True

		return False

	def report_hook(self, block_number: int, block_size: int, total_size: int) -> None:
		"""
		Helper function to fill the progressbar as the image is being downloaded.
		"""
		read_data: int = block_number * block_size

		if total_size > 0:
			self.progressbar.setValue(ceil(read_data * 100 / total_size))
			QApplication.processEvents()

	def run(self) -> None:
		version: str = self.version_list.currentText()

		# Define the source & target paths
		self.source_path = f'https://releases.ubuntu.com/{version}'
		self.target_path = join(abspath(self.target_edit.text()), version)

		# Create the target path if it doesn't exist
		makedirs(self.target_path, exist_ok=True)

		# Get the path to user_data file
		user_data: str = abspath(self.user_edit.text())

		# Validate the YAML code in the user-data file
		self.file_validate(user_data)

		# In order to know the exact version number of the image to download. First, the webpage at
		# the resource path is downloaded. Then the image's file name is extracted from the markup
		# using a regular expression.
		markup: str = urlopen(self.source_path).read().decode('utf-8')
		image_name = search('(ubuntu-\d+\.\d+\.\d+-live-server-amd64\.iso)', markup).group(1)

		# Download the image
		image_path: str = self.image_download(image_name)

		# Download the image's checksum
		checksum_path: str = self.file_download('SHA256SUMS')

		# Download the signature
		signature_path: str = self.file_download('SHA256SUMS.gpg')

		# Download signing key
		self.status_label.setText('Downloading key...')
		gpg = GPG(keyring=join(self.target_path, f'{identity_key}.keyring'))
		gpg.recv_keys('hkp://keyserver.ubuntu.com', identity_key)

		# Verify the signature
		self.status_label.setText('Verifying signature...')
		gpg.verify_data(signature_path, open(checksum_path, 'r').read().encode('utf-8'))

		# Verify image digest
		self.status_label.setText('Verifying image...')
		self.image_verify(checksum_path, image_path)

		# Directory where to store the files extracted from the image
		self.extract_path = join(self.target_path, splitext(image_name)[0])

		# Extract image
		self.status_label.setText('Extracting image...')
		Image(image_path, self.extract_path).extract()

		# If the image supports Hardware Enablement kernel - make use of it
		if self.file_find('boot/grub/grub.cfg', 'hwe-vmlinuz'):
			self.file_replace('boot/grub/grub.cfg', '/casper/initrd', '/casper/hwe-initrd')
			self.file_replace('boot/grub/grub.cfg', '/casper/vmlinuz', '/casper/hwe-vmlinuz')
			self.file_replace('boot/grub/loopback.cfg', '/casper/initrd', '/casper/hwe-initrd')
			self.file_replace('boot/grub/loopback.cfg', '/casper/vmlinuz', '/casper/hwe-vmlinuz')

			if version != 'jammy':
				self.file_replace('isolinux/txt.cfg', '/casper/initrd', '/casper/hwe-initrd')
				self.file_replace('isolinux/txt.cfg', '/casper/vmlinuz', '/casper/hwe-vmlinuz')

		# Add autoinstall parameter
		self.file_replace('boot/grub/grub.cfg', '---', ' autoinstall  ---')
		self.file_replace('boot/grub/loopback.cfg', '---', ' autoinstall  ---')

		if version != 'jammy':
			self.file_replace('isolinux/txt.cfg', '---', ' autoinstall  ---')

		data_path: str = 'nocloud'

		# Add user/meta data files into image
		makedirs(join(self.extract_path, data_path))

		# Copy the user_data file into a directory in the 'data_path' directory
		copyfile(user_data, join(self.extract_path, data_path, 'user-data'))

		# Create an empty meta-data file in the same directory
		with open(join(self.extract_path, data_path, 'meta-data'), 'w') as _:
			pass

		# Modify files to account for the user_data & meta_data files
		self.file_replace('boot/grub/grub.cfg', '---', f'ds=nocloud\\;s=/cdrom/{data_path}/  ---')
		self.file_replace('boot/grub/loopback.cfg', '---', f'ds=nocloud\\;s=/cdrom/{data_path}/  ---')

		if version != 'jammy':
			self.file_replace('isolinux/txt.cfg', '---', f'ds=nocloud;s=/cdrom/{data_path}/  ---')

		# Update the hashes for the file that were altered
		for name in ('./boot/grub/grub.cfg', './boot/grub/loopback.cfg'):
			with open(join(self.extract_path, name), 'r') as file:
				digest: str = md5(file.read()).hexdigest()
				self.file_replace('md5sum.txt', f'^.*  {name}', f'{digest}  {name}')

		# TODO: Package files into new image
		# Image(self.extract_path, image_path.replace('.iso', 'penguinlair.iso')).generate()
